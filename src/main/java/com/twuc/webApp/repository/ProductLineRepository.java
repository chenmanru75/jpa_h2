package com.twuc.webApp.repository;

import com.twuc.webApp.entity.Product;
import com.twuc.webApp.entity.ProductLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductLineRepository extends JpaRepository<ProductLine, String> {
//    List<Product> findByProductLineTextDescriptionContaining(String textDescription);
    List<ProductLine> findByProductLine(String productLine);
}
