package com.twuc.webApp.web;

import com.twuc.webApp.entity.Product;
import com.twuc.webApp.entity.ProductLine;
import com.twuc.webApp.repository.ProductLineRepository;
import com.twuc.webApp.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controller {

    private final ProductRepository productRepository;

    private final ProductLineRepository productLineRepository;

    public Controller(ProductRepository productRepository, ProductLineRepository productLineRepository) {
        this.productRepository = productRepository;
        this.productLineRepository = productLineRepository;
    }

    @GetMapping("/api/products")
    public ResponseEntity<List<Product>> getProducts(){
        return ResponseEntity.ok()
                .body(productRepository.findAll());
    }

    @GetMapping("/api/productlines")
    public ResponseEntity<List<ProductLine>> getProductLines(){
        return ResponseEntity.ok()
                .body(productLineRepository.findAll());
    }

//    @GetMapping("/ap/product-lines")
//    public ResponseEntity<Page<Product>> getPageProducts(Pageable page){
//
//    }
}
