
drop table if exists productlines;

create table if not exists productlines (
  productLine varchar(50) not null ,
  textDescription varchar(4000) default null
);

insert into productlines (productLine, textDescription) values
('Classic Cars', 'Atte'),
('Motorcycles', 'Atte');

drop table if exists products;
CREATE TABLE products (
    productCode varchar(15) not null,
    productName varchar(70) not null,
    productLine varchar(50) not null,
    productScale varchar(10) not null,
    productVendor varchar(50) not null,
    productDescription text not null,
    quantityInStock smallint(6) not null,
    buyPrice decimal(10,2) not null,
    MSRP decimal(10,2) not null
);


insert into products (productCode, productName, productLine, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP) values
('S10_1678', '1969 Harley Davidson Ultimate Chopper', 'Motorcycles', '1:10', 'Min Lin Diecast', 'This replica features working kickstand,n.', 7933, '48.81', '95.70'),
('S10_1949', '1952 Alpine Renault 1300', 'Classic Cars', '1:10', 'Classic Metal Creations', 'Turnable front whee', 7305, '98.58', '214.30'),
('S10_2016', '1996 Moto Guzzi 1100i', 'Motorcycles', '1:10', 'Highway 66 Mini Classics', 'Officingine, woel finish.', 6625, '68.99', '118.94');
