package com.twuc.webApp.web;

import com.twuc.webApp.entity.Product;
import com.twuc.webApp.repository.ProductLineRepository;
import com.twuc.webApp.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.function.Consumer;

public class ProductTest extends JpaTestBase{


    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductLineRepository productLineRepository;

    @Test
    void should_t(){
        flushAndClear(new Consumer<EntityManager>() {
            @Override
            public void accept(EntityManager entityManager) {
                Product productList = productRepository.findByProductCode("S10_1678").orElseThrow(RuntimeException::new);
                System.out.println(productList.toString());
            }
        });

    }
}
