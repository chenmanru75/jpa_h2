package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ProductApiTest extends ApiTestBase {

    @Autowired
    private MockMvc mockMvc;


    @Test
    void should_return_all_products() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/products"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse().getContentAsString();
        System.out.println(")))))))))))))))))))))"+contentAsString);
    }

    @Test
    void should_return_all_productLines() throws Exception {
        String contentAsString = mockMvc.perform(get("/api/productlines"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse().getContentAsString();
        System.out.println(")))))))))))))))))))))"+contentAsString);
    }
}
